#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
import os
import glob
from vex import parse_vex
import psycopg2


class DB(object):

    def __init__(self, driver=None, host=None, port=None, db=None, user=None,
                 password=None):
        self._driver = driver
        self._connection = self._driver.connect(database=db, user=user,
                                                password=password, host=host,
                                                port=port)
        self._cursor = self._connection.cursor()

    def commit(self):
        self._connection.commit()

    def insert(self, table=None, columns=None, values=None):
        """
        Insert into table values in columns.
        """
        assert(len(values) == len(columns))

        columns_str = ' ('
        for column in columns:
            columns_str += column + ', '
        columns_str = columns_str[:-2]
        columns_str += ') '

        values_str = ' ('
        for value in columns:
            values_str += '%s' + ', '
        values_str = values_str[:-2]
        values_str += ') '

        self._cursor.execute('INSERT INTO ' + table + columns_str + ' VALUES' +
                             values_str, tuple(values))

    # TODO: Implement JOIN
    def select(self, table=None, columns=None, where_constraints=None,
               verbose=False):
        """
        Select specified columns from table where other columns equal to some
        specified values.

        :param columns:
            List of column names to retrive.

        :param where_constraints (optional):
            List of tuples (column name, operator, value,) or ``None``.
        """

        columns_str = ''
        if columns:
            for column in columns:
                columns_str += column + ', '
            columns_str = columns_str[:-2]
        else:
            columns_str = '*'

        if where_constraints:
            where_block = ' '
            for constraint in where_constraints:
                where_block += constraint[0] + ' '
                where_block += constraint[1] + ' '
                where_block += constraint[2] + ' '

            self._cursor.execute('SELECT ' + columns_str + ' FROM ' + table +
                                 'WHERE ' + where_block)
        else:
            self._cursor.execute('SELECT ' + columns_str + ' FROM ' + table)

        rows = self._cursor.fetchall()

        if verbose:
            for row in rows:
                for i, column in enumerate(columns):
                    print columns[i] + ' = ', row[i], '\n'

        return rows

    def update(self, table=None, columns=None, values=None,
               where_constraints=None):

        assert(len(values) == len(columns))

        update_str = ' ('
        for column in columns:
            update_str += column + ' = %s, '
        update_str = update_str[:-2]
        update_str += ') '

        print update_str

        self._cursor.execute('UPDATE ' + table + ' SET ' + update_str + 'WHERE ', tuple(values))

    def delete(self, table=None, where_constraints=None):
        """
        Delete rows from table where some specified columns equal to some
        specified values.
        """
        if where_constraints:

            where_block = ''
            for constrain in where_constraints:
                where_block += constrain[0] + ' ' + constrain[1] + ' ' +\
                    constrain[2] + ' AND '
            where_block = where_block[:-5]
            where_block += ';'

            self._cursor.execute('DELETE FROM ' + table + ' WHERE ' +
                                 where_block)
            self._connection.commit()

        elif not where_constraints:

            print 'Deleting all from ' + table
            self._cursor.execute('DELETE ALL FROM ' + table)
            self._connection.commit()


# TODO: put to scripts folder
def insert_vexfiles_2_ra_db_vextable(fnames, return_fnames=False):
    """
    Function that inserts vex-files info to database table.

    Parameter:: iterable of vex-files names.
    """

    vex_inserter = DB(driver=psycopg2, host='odin.asc.rssi.ru', port='5432',
                      db='ra_db', password='1qa2ws', user='lipa')

    inserted_files = list()
    failed_files = list()

    for fname in fnames:
        try:
            ivalues = parse_vex(fname)
        except SyntaxError, AttributeError:
            print 'Error inserting: ' + fname
            failed_files.append(fname)
            continue
        try:
            for values in ivalues:
                    vex_inserter.insert(table='database_vex',
                                        columns=['obscode', 'source',
                                                 'telescope', 'scan', 'start',
                                                 'duration', 'freq', 'bw',
                                                 'chan', 'bbc', 'pol'],
                                        values=values)
            print 'Inserted: ' + fname
            inserted_files.append(fname)
        except SyntaxError:
            print 'Error inserting: ' + fname
            failed_files.append(fname)

    vex_inserter._connection.commit()

    if return_fnames:
        return list(set(inserted_files)), list(set(failed_files))


if __name__ == '__main__':

    vex_inserter = DB(driver=psycopg2, host='odin.asc.rssi.ru', port='5432',
                      db='ra_db', password='1qa2ws', user='lipa')
    vex_path = '/home/ilya/code/asc/asc/'
    vex_files = glob.glob(os.path.join(vex_path, "*.vex"))
    inserted, failed = insert_vexfiles_2_ra_db_vextable(vex_files,
                                                        return_fnames=True)
