import ply.lex as lex
import ply.yacc as yacc

from MultiDict import MultiDict
from datetime import datetime, timedelta

reserved = {
    'def': 'DEF',
    'enddef': 'ENDDEF',
    'scan': 'SCAN',
    'endscan': 'ENDSCAN',
    'ref': 'REF',
}

# List of token names.
tokens = [
    'COLON',
    'SEMICOLON',
    'EQ',
    'DOLLAR',
    'IDENT',
] + list(reserved.values())

# Regular expressions rules for simple tokens
t_COLON     = r':'
t_SEMICOLON = r';'
t_EQ        = r'='
t_DOLLAR    = r'\$'

def t_IDENT(t):
    r'[^&$\*:;=\t\n\r ]+'
    t.type = reserved.get(t.value, 'IDENT')
    return t

def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)
    return

def t_error(t):
    print "Illegal character '%s'" % t.value[0]
    t.lexer.skip(1)
    return

t_ignore_COMMENT = r'\*.*'
t_ignore = ' \t&'

lexer = lex.lex()

def p_vex(t):
    'vex : vex_header blocks'
    t[0] = t[2]
    return

def p_vex_header(t):
    'vex_header : IDENT EQ IDENT SEMICOLON'
    t[0] = MultiDict()
    t[0][t[1]] = t[3]
    return

def p_blocks(t):
    '''blocks : block
              | blocks block'''
    t[0] = t[1]
    if len(t) > 2:
        t[0].update(t[2])
        pass
    return

def p_block(t):
    'block : block_header block_content'
    t[0] = MultiDict()
    t[0][str(t[1])] = t[2]
    return

def p_block_header(t):
    'block_header : DOLLAR IDENT SEMICOLON'
    t[0] = t[2]
    return

def p_block_content(t):
    '''block_content : block_lines
                     | block_content def_block
                     | block_content scan_block'''
    t[0] = t[1]
    if len(t) > 2:
        t[0].update(t[2])
        pass
    return

def p_def_block(t):
    'def_block : DEF word SEMICOLON block_lines ENDDEF SEMICOLON'
    t[0] = MultiDict()
    t[0][str(t[2])] = t[4]
    return

def p_scan_block(t):
    'scan_block : SCAN word SEMICOLON block_lines ENDSCAN SEMICOLON'
    t[0] = MultiDict()
    t[0][str(t[2])] = t[4]
    return

def p_block_lines(t):
    '''block_lines :
                   | block_lines block_line'''
    if len(t) == 1:
        t[0] = MultiDict()
    else:
        t[0] = t[1]
        t[0].update(t[2])
        pass
    return

def p_block_line(t):
    '''block_line : REF DOLLAR word EQ value SEMICOLON
                  | word EQ value SEMICOLON'''
    t[0] = MultiDict()
    if t[1] == "ref":
        t[0][str(t[3])] = t[5]
    else:
        t[0][str(t[1])] = t[3]
        pass
    return

def p_value(t):
    '''value : word
             |
             | value COLON word
             | value COLON'''
    if len(t) == 1:
        t[0] = ""
    elif len(t) == 2:
        t[0] = t[1]
    elif len(t) == 3:
        t[0] = t[1]
    else:
        try:
            t[0] = t[1]
            t[0].append(t[3])
        except:
            t[0] = [t[1], t[3]]
            pass
        pass
    return

def p_word(t):
    '''word : IDENT
            | word IDENT'''
    if len(t) == 2:
        t[0] = t[1]
    else:
        t[0] = str(t[1]) + ' ' + str(t[2])
        pass
    return

def p_error(t):
    raise SyntaxError, "at line %d, token %s" % (t.lineno, t.value)

parser = yacc.yacc(debug=0)

def parse(s):
    return parser.parse(s, lexer=lexer)

def Vex(file):
    fp = open(file, 'r')
    vex = fp.read()
    fp.close()
    return parse(vex)


def parse_vex(fname):
    """
    Function that parses vex-files and returns data to be inserted to ra_db.
    """

    results = list()
    out = Vex(fname)

    # Find experiment code and check it is one
    assert(len(out['EXPER'].items()) == 1)
    obscode = out['EXPER'].items()[0][0]

    scans = list()
    # Populate list of scan#
    for key in out['SCHED'].iterkeys():
        scans.append(key)

    # Populate each scan with info
    # Loop for earch scan
    for scan in scans:
        mode = out['SCHED'][scan]['mode']
        start = out['SCHED'][scan]['start']
        source = out['SCHED'][scan]['source']

        # Loop for each telescope in scan
        for tel in [tel[0] for tel in
                    out['SCHED'][scan].getall('station')]:
            length = [station[2] for station in
                      out['SCHED'][scan].getall('station') if station[0] ==
                                                              tel][0]
            freq_setup = [freq[0] for freq in out['MODE'][mode].getall('FREQ')
                          if tel in freq][0]
            if_setup = [freq[0] for freq in out['MODE'][mode].getall('IF')
                        if tel in freq][0]
            bbc = [bbcs[0] for bbcs in out['MODE'][mode].getall('BBC') if tel
            in bbcs][0]
            # IF = [res[0] for res in out['MODE'][mode].getall('IF') if res[1]
            #       == tel]
            # Loop for each channel
            for chan in [chans_info[4] for chans_info in
                         out['FREQ'][freq_setup].getall('chan_def')]:
                chan_frequency = \
                    [chans_info[1] for chans_info in
                     out['FREQ'][freq_setup].getall('chan_def')
                     if chans_info[4] == chan][0]
                chan_LU = [chans_info[2] for chans_info in
                           out['FREQ'][freq_setup].getall('chan_def')
                           if chans_info[4] == chan][0]
                chan_BW = [chans_info[3] for chans_info in
                           out['FREQ'][freq_setup].getall('chan_def')
                           if chans_info[4] == chan][0]
                chan_BBC = [chans_info[5] for chans_info in
                            out['FREQ'][freq_setup].getall('chan_def')
                            if chans_info[4] == chan][0]

                # Decrypt BBC:
                num, IF_type = [(res[1], res[2]) for res in
                                out['BBC'][bbc].getall('BBC_assign') if
                                res[0] == chan_BBC][0]
                pol = [res[2] for res in out['IF'][if_setup].getall('if_def')
                       if res[0] == IF_type][0]
                # It isn't U/L
                #ul = [res[4] for res in out['IF'][if_setup].getall('if_def') if res[0] == IF_type][0]

                #print scan, tel, start, length, freq_setup, if_setup, bbc,\
                #        chan, chan_frequency, chan_LU, chan_BW, chan_BBC, num, IF_type,\
                #pol
                result = list()

                # Obscode is ``string``
                obscode_str = obscode
                # Scan number to ``int``
                scan_int = int(scan[2:])
                # source is ``string``
                source_str = source
                # Telescope is ``string``
                tel_str = tel
                # Starttime of scan to ``datetime``
                fmt = '%Yy%jd%Hh%Mm%Ss'
                start_dt = datetime.strptime(start, fmt)
                #start_str = start_dt.strftime('%Y-%m-%d %H:%M:%S')
                # Length of scan to timedelta
                length_dt = timedelta(seconds=float(length.split()[0]))
                # Channel # to ``int``
                chan_int = int(chan[-1])
                # Sky frequency to float
                chan_frequency_fl = float(chan_frequency.split()[0])
                # Lower/Upper channelis ``string``
                chan_LU_str = chan_LU
                # Channel bandwidth to ``float``
                chan_BW_fl = float(chan_BW.split()[0])
                # Polarization is ``string``
                pol_str = pol

                result = obscode_str, source_str, tel_str, scan_int, start_dt, \
                         length_dt, chan_frequency_fl, chan_BW_fl, chan_int, \
                         chan_LU_str, pol_str
                results.append(result)
    return results
