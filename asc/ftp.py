#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from stat import S_ISDIR

import ftplib
import glob
try:
    import paramiko
    is_imported_paramiko = True
except ImportError:
    is_imported_paramiko = False


class FTPWrapper(object):

    def __init__(self, host=None, port=None, username=None, password=None,
                 sftp=False):
        if sftp:
            self._ftp = SFTP(host=host, port=port, username=username,
                            password=password)
        else:
            self._ftp = FTP(host=host, username=username, password=password)

    def list_files(self, remote_path):
        return  self._ftp.list_files(remote_path)

    def list_dirs(self, remote_path):
        return self._ftp.list_dirs(remote_path)

    #TODO: If `fnames` is None then download all files from the `remote_path`
    def get_files_from_dir(self, card=None, remote_path=None,
                           local_path=None):
        self._ftp.get_files_from_dir(card=card,
                                     remote_path=remote_path,
                                     local_path=local_path)

    #TODO: If `fnames` is None then download all files from nested directories
    def get_files_from_nested_dirs(self, card=None, remote_path=None,
                                   local_path=None):
        print "getting files from ", remote_path
        print self.list_files(remote_path=remote_path)
        self.get_files_from_dir(card=card, remote_path=remote_path,
                    local_path=local_path)
        dirs = self.list_dirs(remote_path)
        print "Dirs to traverse ", dirs
        for dir_ in dirs:
            remote_path_ = remote_path + "/" + dir_
            print "getting files from ", remote_path_
            print self.list_files(remote_path=remote_path_)
            self.get_files_from_dir(card=card, remote_path=remote_path_,
                                    local_path=local_path)
            self.get_files_from_nested_dirs(card=card, remote_path=remote_path_,
                                local_path=local_path)

    def put_files_to_dir(self, fnames=None, remote_path=None):
        self._ftp.put_files_to_dir(fnames=fnames, remote_path=remote_path)


class FTP(object):

    def __init__(self, host=None, username=None, password=None):
        self._ftp = ftplib.FTP(host, username, passwd=password)

    def list_dirs(self, remote_path):

        dirs = list()

        def _lambda(line):
            if line[0] == 'd':
                dirs.append(line.split()[-1])

        self._ftp.cwd(remote_path)
        self._ftp.dir('-d', _lambda)
        try:
            dirs.remove('.')
            dirs.remove('..')
        except ValueError:
            pass
        if not dirs:
            print 'No sudirectories in ' + remote_path

        return dirs

    def list_files(self, remote_path):
        return self._ftp.nlst(remote_path)

    def get_files_from_dir(self, card=None, remote_path=None,
                           local_path=None):
        """
        :param card:
            ".vex"
        :param remote_path:
        :param local_path:
        :return:
        """

        self._ftp.cwd(remote_path)
        files = self.list_files(remote_path)
        # Now check if fnames are in files and download them
        # file_ - remote file
        for file_ in files:
            if card in file_:
                print 'Downloading ' + file_
                with open(file_, "wb") as vexfile:
                    self._ftp.retrbinary('RETR %s' % file_, vexfile.write)


class SFTP(object):

    def __init__(self, host=None, username=None, password=None, port=None):
        if not is_imported_paramiko:
            raise Exception('Install `paramiko` for sftp support!')

        self._transport = paramiko.Transport((host, port))
        self._transport.connect(username=username, password=password)
        self._sftp = paramiko.SFTPClient.from_transport(self._transport)

    def isdir(self, path):
        try:
            return S_ISDIR(self._sftp.stat(path).st_mode)
        except IOError:
            #Path does not exist, so by definition not a directory
            return False

    def list_dirs(self, remote_path):

        dirs = list()
        entries = self._sftp.listdir(path=remote_path)

        for entry in entries:
            if self.isdir(remote_path + entry):
                dirs.append(entry)

        return dirs

    def list_files(self, remote_path):

        files = list()
        entries = self._sftp.listdir(remote_path)

        for entry in entries:
            if not self.isdir(remote_path + entry):
                files.append(entry)

        return files

    def get_files_from_dir(self, fnames=None, remote_path=None,
                           local_path=None):

        files = self.list_files(remote_path)
        # Now check if fnames are in files and download them
        self._sftp.chdir(remote_path)
        for fname in files:
            self._sftp.get(fname, local_path)


if __name__ == '__main__':
    remote_path = '/schedule/grtsched'
    ftpw = FTPWrapper(host='webinet.asc.rssi.ru',username='ilia_p',
                      password='r%jlzhf_@')
    ftpw.get_files_from_nested_dirs('.vex', remote_path)


