asc
=====

**Tools for automating post-correlation data processing at ASC**

asc is collection of tools used for automating post-correlation Radioastron
data processing at ASC LPI.

Documentation
-------------

TBA

License
-------

Copyright 2014 Ilya Pashchenko.

asc is free software made available under the MIT License. For details see
the LICENSE.txt file.
